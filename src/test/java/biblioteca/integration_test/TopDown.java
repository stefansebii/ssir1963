package biblioteca.integration_test;

import biblioteca.control.BibliotecaCtrl;
import biblioteca.model.Carte;
import biblioteca.repository.repo.CartiRepo;
import biblioteca.repository.repoInterfaces.CartiRepoInterface;
import biblioteca.unit_test.CartiRepoMock;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * Created by Sebi on 21-Apr-18.
 */
public class TopDown {
    private CartiRepoInterface repo;
    private BibliotecaCtrl controller;


    @Test
    public void testUnitA() throws Exception{
        repo = new CartiRepoMock();
        controller = new BibliotecaCtrl(repo);

        Carte c = new Carte();
        c.setTitlu("Alta carte");
        c.setReferenti(Collections.singletonList("Autor"));
        c.setAnAparitie("2013");
        c.setEditura("Editura fantastica");
        c.setCuvinteCheie(Collections.singletonList("faina"));

        controller.adaugaCarte(c);
        assertTrue(controller.getCarti().contains(c));
    }

    @Test
    public void testIntegrationAB() throws Exception {
        CartiRepo fileRepo = new CartiRepo();
        fileRepo.setFile("testDB.dat");
        fileRepo.resetDatabase();

        repo = fileRepo;
        populateRepo(repo);
        controller = new BibliotecaCtrl(repo);

        Carte c = new Carte();
        c.setTitlu("Alta carte");
        c.setReferenti(Collections.singletonList("Autor"));
        c.setAnAparitie("2014");
        c.setEditura("Editura fantastica");
        c.setCuvinteCheie(Collections.singletonList("faina"));

        controller.adaugaCarte(c);
        assertTrue(controller.getCarti().contains(c));

        List<Carte> carti = controller.cautaCarte("Autor");
        assertTrue(carti.size() == 1);
    }

    @Test
    public void testIntegrationABC() throws Exception {
        CartiRepo fileRepo = new CartiRepo();
        fileRepo.setFile("testDB.dat");
        fileRepo.resetDatabase();

        repo = fileRepo;
        populateRepo(repo);
        controller = new BibliotecaCtrl(repo);

        Carte c = new Carte();
        c.setTitlu("Alta carte");
        c.setReferenti(Collections.singletonList("Autor"));
        c.setAnAparitie("2014");
        c.setEditura("Editura fantastica");
        c.setCuvinteCheie(Collections.singletonList("faina"));

        controller.adaugaCarte(c);
        assertTrue(controller.getCarti().contains(c));

        List<Carte> carti = controller.cautaCarte("Autor");
        assertTrue(carti.size() == 1);

        carti = controller.getCartiOrdonateDinAnul("2014");
        assertTrue(carti.size() == 3);
    }

    @SuppressWarnings("Duplicates")
    private void populateRepo(CartiRepoInterface repo) throws Exception{
        Carte c1 = new Carte();
        c1.setTitlu("The Fellowship of the Ring");
        c1.setReferenti(Collections.singletonList("J.R.R. Tolkien"));
        c1.setEditura("George Allen & Unwin");
        c1.setAnAparitie("1954");
        c1.setCuvinteCheie(Arrays.asList("fantasy", "epic"));
        repo.adaugaCarte(c1);

        Carte c2 = new Carte();
        c2.setTitlu("Mr. Mercedes");
        c2.setReferenti(Collections.singletonList("Stephen King"));
        c2.setAnAparitie("2014");
        c2.setCuvinteCheie(Collections.singletonList("thriller"));
        c2.setEditura("Hodder & Stoughton");
        repo.adaugaCarte(c2);

        Carte c3 = new Carte();
        c3.setTitlu("Good Omens");
        c3.setReferenti(Arrays.asList("Terry Pratchet", "Neil Gaiman"));
        c3.setEditura("HarperCollins");
        c3.setAnAparitie("2014");
        c3.setCuvinteCheie(Collections.singletonList("comedy"));
        repo.adaugaCarte(c3);
    }
}
