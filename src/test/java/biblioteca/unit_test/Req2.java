package biblioteca.unit_test;

import biblioteca.model.Carte;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;

public class Req2 {

    private CartiRepoMock repo;

    @Before
    public void setUp(){
        repo = new CartiRepoMock();
    }

    @Test
    public void tc1() throws Exception {
        Carte c = getValidBook();
        c.setReferenti(Arrays.asList("J.R.R. Tolkien"));
        c.setTitlu("1");
        repo.adaugaCarte(c);

        List<Carte> result = repo.cautaCarte("Tolkien");
        assertTrue(result.size() == 1);
        assertTrue(result.get(0).getTitlu().equals("1"));
    }

    @Test
    public void tc2() throws Exception {
        List<Carte> result = repo.cautaCarte("Tolkien");
        assertTrue(result.size() == 0);
    }

    @Test
    public void tc3() throws Exception {
        Carte c1 = getValidBook();
        c1.setReferenti(Arrays.asList("J.R.R. Tolkien"));
        c1.setTitlu("1");
        repo.adaugaCarte(c1);

        Carte c2 = getValidBook();
        c2.setReferenti(Arrays.asList("Orwell"));
        c2.setTitlu("2");
        repo.adaugaCarte(c2);

        Carte c3 = getValidBook();
        c3.setReferenti(Arrays.asList("George R.R. Martin","Christopher Tolkien"));
        c3.setTitlu("3");
        repo.adaugaCarte(c3);

        List<Carte> result = repo.cautaCarte("Tolkien");
        assertTrue(result.size() == 2);
        assertTrue(result.contains(c1));
        assertTrue(result.contains(c3));
    }

    @Test
    public void tc4() throws Exception {
        Carte c = getValidBook();
        c.setReferenti(new LinkedList<String>());
        c.setTitlu("1");
        repo.adaugaCarte(c);

        List<Carte> result = repo.cautaCarte("Tolkien");
        assertTrue(result.size() == 0);
    }

    @Test
    public void tc5() throws Exception {
        Carte c = getValidBook();
        c.setReferenti(Arrays.asList("Philip K. Dick", "Tolkien", "Stephen King"));
        c.setTitlu("1");
        repo.adaugaCarte(c);

        List<Carte> result = repo.cautaCarte("Tolkien");
        assertTrue(result.size() == 1);
        assertTrue(result.contains(c));
    }

    @Test(expected = NullPointerException.class)
    public void tc6() throws Exception {
        Carte c = getValidBook();
        c.setReferenti(new LinkedList<String>());
        c.setTitlu("1");
        repo.adaugaCarte(c);

        repo.adaugaCarte(null);
        repo.adaugaCarte(null);

        repo.cautaCarte("Tolkien");
    }


    private static final String VALID_TITLE = "The Two Towers";
    private static final String VALID_YEAR = "1954";
    private static final String VALID_PUBLISHER = "George Allen & Unwin";
    private static final List<String> VALID_KEYWORDS = Arrays.asList("fantasy", "epic");
    private static final List<String> VALID_AUTHORS = Collections.singletonList("J.R.R. Tolkien");

    private Carte getValidBook(){
        Carte carte = new Carte();
        carte.setTitlu(VALID_TITLE);
        carte.setEditura(VALID_PUBLISHER);
        carte.setCuvinteCheie(VALID_KEYWORDS);
        carte.setAnAparitie(VALID_YEAR);
        carte.setReferenti(VALID_AUTHORS);
        return carte;
    }
}