package biblioteca.unit_test;

import biblioteca.model.Carte;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class Req3 {
    private CartiRepoMock repo;

    @Before
    public void setUp() {
        repo = new CartiRepoMock();
        populateMockRepo();
    }

    @Test
    public void validTest(){
        List<Carte> res = repo.getCartiOrdonateDinAnul("2014");
        assertEquals(res.size(), 2);
        assertEquals(res.get(0).getTitlu(), "Good Omens");
        assertEquals(res.get(1).getTitlu(), "Mr. Mercedes");
    }

    @Test
    public void invalidTest(){
        List<Carte> res = repo.getCartiOrdonateDinAnul(null);
        assertEquals(res.size(), 0);
    }

    private void populateMockRepo(){
        Carte c1 = new Carte();
        c1.setTitlu("The Fellowship of the Ring");
        c1.setReferenti(Collections.singletonList("J.R.R. Tolkien"));
        c1.setEditura("George Allen & Unwin");
        c1.setAnAparitie("1954");
        c1.setCuvinteCheie(Arrays.asList("fantasy", "epic"));
        repo.adaugaCarte(c1);

        Carte c2 = new Carte();
        c2.setTitlu("Mr. Mercedes");
        c2.setReferenti(Collections.singletonList("Stephen King"));
        c2.setAnAparitie("2014");
        c2.setCuvinteCheie(Collections.singletonList("thriller"));
        c2.setEditura("Hodder & Stoughton");
        repo.adaugaCarte(c2);

        Carte c3 = new Carte();
        c3.setTitlu("Good Omens");
        c3.setReferenti(Arrays.asList("Terry Pratchet", "Neil Gaiman"));
        c3.setEditura("HarperCollins");
        c3.setAnAparitie("2014");
        c3.setCuvinteCheie(Collections.singletonList("comedy"));
        repo.adaugaCarte(c3);
    }
}
