package biblioteca.unit_test;

import biblioteca.control.BibliotecaCtrl;
import biblioteca.model.Carte;
import biblioteca.repository.repoInterfaces.CartiRepoInterface;
import biblioteca.unit_test.CartiRepoMock;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * Created by Sebi on 24-Mar-18.
 */
public class Req1 {
    private static final String VALID_TITLE = "The Two Towers";
    private static final String VALID_YEAR = "1954";
    private static final String VALID_PUBLISHER = "George Allen & Unwin";
    private static final List<String> VALID_KEYWORDS = Arrays.asList("fantasy", "epic");
    private static final List<String> VALID_AUTHORS = Collections.singletonList("J.R.R. Tolkien");

    private Carte TEST_BOOK;

    private CartiRepoInterface repository;
    private BibliotecaCtrl controller;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setUp(){
        repository = new CartiRepoMock();
        controller = new BibliotecaCtrl(repository);

        TEST_BOOK = new Carte();
        TEST_BOOK.setTitlu(VALID_TITLE);
        TEST_BOOK.setAnAparitie(VALID_YEAR);
        TEST_BOOK.setEditura(VALID_PUBLISHER);
        TEST_BOOK.setCuvinteCheie(VALID_KEYWORDS);
        TEST_BOOK.setReferenti(VALID_AUTHORS);
    }

    // utility method
    private String repeat(int count, String with) {
        return new String(new char[count]).replace("\0", with);
    }

    private void checkValidAdd(Carte carte) throws Exception {
        int before = controller.getCarti().size();

        controller.adaugaCarte(carte);

        int after = controller.getCarti().size();
        assertTrue(after - before == 1);
    }

    @Test
    public void tc1_ecp() throws Exception {
        checkValidAdd(TEST_BOOK);
    }

    @Test
    public void tc4_ecp() throws Exception {
        expectedException.expect(Exception.class);
        expectedException.expectMessage("An invalid");

        TEST_BOOK.setAnAparitie("-1");
        controller.adaugaCarte(TEST_BOOK);
    }

    @Test
    public void tc5_ecp() throws Exception {
        expectedException.expect(Exception.class);
        expectedException.expectMessage("An invalid");

        TEST_BOOK.setAnAparitie("2019");
        controller.adaugaCarte(TEST_BOOK);
    }

    @Test
    public void tc6_ecp() throws Exception {
        expectedException.expect(Exception.class);
        expectedException.expectMessage("Titlu invalid");

        TEST_BOOK.setTitlu("");
        controller.adaugaCarte(TEST_BOOK);
    }

    @Test
    public void tc7_ecp() throws Exception {
        expectedException.expect(Exception.class);
        expectedException.expectMessage("Lista cuvinte cheie vida!");

        TEST_BOOK.setCuvinteCheie(null);
        controller.adaugaCarte(TEST_BOOK);
    }

    @Test
    public void tc8_ecp() throws Exception {
        expectedException.expect(Exception.class);
        expectedException.expectMessage("Lista autori vida!");

        TEST_BOOK.setReferenti(null);
        controller.adaugaCarte(TEST_BOOK);
    }

    @Test
    public void tc9_ecp() throws Exception {
        expectedException.expect(Exception.class);
        expectedException.expectMessage("Autor invalid");

        TEST_BOOK.setReferenti(Collections.singletonList("J.R.R. Tolkien123"));
        controller.adaugaCarte(TEST_BOOK);
    }

    @Test
    public void tc10_ecp() throws Exception {
        expectedException.expect(Exception.class);
        expectedException.expectMessage("Titlu invalid!");

        TEST_BOOK.setTitlu(repeat(1001, "M"));
        controller.adaugaCarte(TEST_BOOK);
    }

    @Test
    public void tc12_ecp() throws Exception {
        expectedException.expect(Exception.class);
        expectedException.expectMessage("Editura invalida");

        TEST_BOOK.setEditura(repeat(1001, "M"));
        controller.adaugaCarte(TEST_BOOK);
    }

    @Test
    public void tc1_bva() throws Exception {
        expectedException.expect(Exception.class);
        expectedException.expectMessage("Titlu invalid");

        TEST_BOOK.setTitlu("");
        controller.adaugaCarte(TEST_BOOK);
    }

    @Test
    public void tc2_bva() throws Exception {
        TEST_BOOK.setTitlu("M");
        checkValidAdd(TEST_BOOK);
    }

    @Test
    public void tc3_bva() throws Exception {
        TEST_BOOK.setTitlu("MM");
        checkValidAdd(TEST_BOOK);
    }

    @Test
    public void tc4_bva() throws Exception {
        TEST_BOOK.setTitlu(repeat(1000, "M"));
        checkValidAdd(TEST_BOOK);
    }

    @Test
    public void tc5_bva() throws Exception {
        TEST_BOOK.setTitlu(repeat(999, "M"));
        checkValidAdd(TEST_BOOK);
    }

    @Test
    public void tc7_bva() throws Exception {
        TEST_BOOK.setEditura("");
        checkValidAdd(TEST_BOOK);
    }

    @Test
    public void tc9_bva() throws Exception {
        TEST_BOOK.setEditura("D");
        checkValidAdd(TEST_BOOK);
    }

    @Test
    public void tc10_bva() throws Exception {
        TEST_BOOK.setEditura(repeat(1000, "D"));
        checkValidAdd(TEST_BOOK);
    }

    @Test
    public void tc11_bva() throws Exception {
        TEST_BOOK.setEditura(repeat(999, "D"));
        checkValidAdd(TEST_BOOK);
    }

    @Test
    public void tc13_bva() throws Exception {
        TEST_BOOK.setAnAparitie("0");
        checkValidAdd(TEST_BOOK);
    }

    @Test
    public void tc14_bva() throws Exception {
        TEST_BOOK.setAnAparitie("1");
        checkValidAdd(TEST_BOOK);
    }

    @Test
    public void tc16_bva() throws Exception {
        TEST_BOOK.setAnAparitie("2018");
        checkValidAdd(TEST_BOOK);
    }

    @Test
    public void tc17_bva() throws Exception {
        expectedException.expect(Exception.class);
        expectedException.expectMessage("An invalid");

        TEST_BOOK.setAnAparitie("2019");
        controller.adaugaCarte(TEST_BOOK);
    }

    @Test
    public void tc19_bva() throws Exception {
        TEST_BOOK.setReferenti(new LinkedList<String>());
        checkValidAdd(TEST_BOOK);
    }

    @Test
    public void tc22_bva() throws Exception {
        TEST_BOOK.setCuvinteCheie(new LinkedList<String>());
        checkValidAdd(TEST_BOOK);
    }

    @Test
    public void tc23_bva() throws Exception {
        TEST_BOOK.setCuvinteCheie(Collections.singletonList("fantasy"));
        checkValidAdd(TEST_BOOK);
    }
}