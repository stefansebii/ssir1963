package biblioteca.repository;

/**
 * Created by Sebi on 09-Mar-18.
 */
public class RepositoryException extends Exception {
    public RepositoryException(String message){
        super(message);
    }
}
