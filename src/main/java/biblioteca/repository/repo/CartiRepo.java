package biblioteca.repository.repo;


import biblioteca.model.Carte;
import biblioteca.repository.RepositoryException;
import biblioteca.repository.repoInterfaces.CartiRepoInterface;
import com.sun.org.apache.regexp.internal.RE;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Repository pentru carti bazat pe fisiere
 */
public class CartiRepo implements CartiRepoInterface {
	
	private String file = "cartiBD.dat";
	
	public CartiRepo(){
		URL location = CartiRepo.class.getProtectionDomain().getCodeSource().getLocation();
        System.out.println(location.getFile());
	}

	public void setFile(String file){
		this.file = file;
	}

	public void resetDatabase() throws RepositoryException{
		try {
			PrintWriter writer = new PrintWriter(file);
			writer.print("");
			writer.close();
		} catch (FileNotFoundException e) {
			throw new RepositoryException(e.getMessage());
		}
	}
	
	@Override
	public void adaugaCarte(Carte c) throws RepositoryException{
		BufferedWriter bw = null;
		try {
			bw = new BufferedWriter(new FileWriter(file,true));
			bw.write(c.toString());
			bw.newLine();
			
			bw.close();
		} catch (IOException e) {
			throw new RepositoryException(e.getMessage());
		}
	}

	@Override
	public List<Carte> getCarti() throws RepositoryException{
		List<Carte> lc = new ArrayList<Carte>();
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(file));
			String line = null;
			while((line=br.readLine())!=null){
				lc.add(Carte.getCarteFromString(line));
			}
			
			br.close();
		} catch (FileNotFoundException e) {
			throw new RepositoryException("File not found");
		} catch (IOException e) {
			throw new RepositoryException(e.getMessage());
		}
		
		return lc;
	}

	@Override
	public void modificaCarte(Carte nou, Carte vechi) throws RepositoryException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void stergeCarte(Carte c) throws RepositoryException{
		// TODO Auto-generated method stub
	}

	@Override
	public List<Carte> cautaCarte(String ref) throws RepositoryException{
		List<Carte> carti = getCarti();
		List<Carte> cartiGasite = new ArrayList<Carte>();
		int i=0;
		while (i<carti.size()){
			boolean flag = false;
			List<String> lref = carti.get(i).getReferenti();
			int j = 0;
			while(j<lref.size()){
				if(lref.get(j).toLowerCase().contains(ref.toLowerCase())){
					flag = true;
					break;
				}
				j++;
			}
			if(flag == true){
				cartiGasite.add(carti.get(i));
			}
			i++;
		}
		return cartiGasite;
	}

	@Override
	public List<Carte> getCartiOrdonateDinAnul(String an) throws RepositoryException{
		List<Carte> lc = getCarti();
		List<Carte> lca = new ArrayList<Carte>();
		for(Carte c:lc){
			if(c.getAnAparitie().equals(an)){
				lca.add(c);
			}
		}
		
		Collections.sort(lca,new Comparator<Carte>(){

			@Override
			public int compare(Carte a, Carte b) {
				if(a.getTitlu().compareTo(b.getTitlu())==0){
					int minAutori = Math.min(a.getReferenti().size(), b.getReferenti().size());
					for (int i = 0; i < minAutori; i++){
						int compareResult = a.getReferenti().get(i).compareTo(b.getReferenti().get(i));
						if (compareResult != 0){
							return compareResult;
						}
					}
					return a.getReferenti().size() < b.getReferenti().size() ? -1 : 1;
				}

				return a.getTitlu().compareTo(b.getTitlu());
			}
		
		});
		
		return lca;
	}

}
