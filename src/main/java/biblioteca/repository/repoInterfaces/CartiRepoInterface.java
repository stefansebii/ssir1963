package biblioteca.repository.repoInterfaces;


import biblioteca.model.Carte;
import biblioteca.repository.RepositoryException;

import java.util.List;

/**
 * Interfata repository-ului pentru carti
 */
public interface CartiRepoInterface {
	void adaugaCarte(Carte c) throws RepositoryException;
	void modificaCarte(Carte nou, Carte vechi) throws RepositoryException;
	void stergeCarte(Carte c) throws RepositoryException;
	List<Carte> cautaCarte(String ref) throws RepositoryException;
	List<Carte> getCarti() throws RepositoryException;
	List<Carte> getCartiOrdonateDinAnul(String an) throws RepositoryException;
}
